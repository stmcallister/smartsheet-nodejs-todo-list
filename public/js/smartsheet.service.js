(function() {
    'use strict';

    angular.module('crossoff')
        .factory('smartsheetService', ['$http', '$rootScope', '$filter', smartsheetService] );

    function smartsheetService($http, $rootScope, $filter) {
        var baseURL = '/api/';
        var smartsheetBaseUrl = baseURL + 'smartsheet/';
        var sheetURL = smartsheetBaseUrl + 'sheet/';
        
        // model for currentWorkflow
        var service = {
            getSheet : getSheet,
            addRow : addRow,
            updateRow : updateRow
        };

        return service;

        function addRow(sheetId, task) {
            return $http.post(sheetURL + sheetId + "/row", {
                value: task
            }).then(null, errorHandler);
        }
        function updateRow(sheetId, task) {
            return $http.put(sheetURL + sheetId + "/row/" + task.rowId, {
                value: task
            }).then(null, errorHandler);
        }
        function getSheet(sheetId) {
            return $http.get(sheetURL + sheetId).then(null, errorHandler);
        }

        function errorHandler(reason) {
            return reason;
        }
    }

})();
