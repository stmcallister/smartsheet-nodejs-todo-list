(function() {
    'use strict';

    angular
        .module('crossoff')
        .directive('crossoff', ['smartsheetService', crossoff]);

    function crossoff(smartsheetService) {
        var crossoff = {
            restrict: 'E',
            scope: {},
            templateUrl: '/public/js/crossoff.html',
            controller: crossoffController,
            controllerAs: 'vm',
            bindToController: true
        };

        return crossoff;

        function crossoffController($scope) {
            var vm = this;
            vm.tasks = [];
            vm.sheetId = "put_your_sheet_id_here";

            vm.getTasks = function() {
                vm.isLoading = true;
                vm.tasks = [];
                
                smartsheetService.getSheet(vm.sheetId).then(function (response) {
                    vm.isLoading = false;
                    if (response && response.data) {
                        var primaryColumnId = vm.findPrimaryColumnId(response.data.columns);
                        var checkboxColumnId = vm.findCheckboxColumnId(response.data.columns);
                        var rows = response.data.rows;
                        
                        for (var i = 0; i < rows.length; i++) {
                            var row = rows[i];
                            var task = {
                                isDone: false,
                                value: '',
                                rowId: row.id
                            };

                            for (var y = 0; y < row.cells.length; y++) {
                                var cell = row.cells[y];

                                if (cell.columnId === primaryColumnId) {
                                    task.value = cell.value;
                                }
                                if (cell.columnId === checkboxColumnId 
                                    && cell.value) {
                                    task.isDone = cell.value;
                                }
                            }
                            vm.tasks.push({task});
                        } 
                    }
                });
            }
            vm.getTasks();
            vm.findPrimaryColumnId = function(columns) {
                for (var x = 0; x < columns.length; x++) {
                    if (columns[x].primary === true) {
                        return columns[x].id;
                    }
                }
            }
            vm.findCheckboxColumnId = function(columns) {
                for (var x = 0; x < columns.length; x++) {
                    if (columns[x].type === 'CHECKBOX') {
                        return columns[x].id;
                    }
                }
            }

            vm.createTask = function() {
                if (vm.formData.text != '') {
                    smartsheetService.addRow(vm.sheetId, vm.formData.text)
                    .then(function(response) {
                        if (response.data && response.data.message 
                            && response.data.message === "SUCCESS") {
                            initPage();
                        }
                    });
                }
            }
            vm.updateTask = function(task) {
                smartsheetService.updateRow(vm.sheetId, task)
                .then(function(response) {
                    if (response.data && response.data.message 
                        && response.data.message === "SUCCESS") {
                        initPage();
                    }
                });
            }
            function initPage() {
                if (vm.formData && vm.formData.text) {
                    vm.formData.text = '';
                }
                vm.getTasks();
            }
        }
    }
})();
