'use strict';
const express = require('express');
const client = require('Smartsheet');
const bodyParser = require('body-parser');   

const accessToken = process.env.SMARTSHEET_ACCESS_TOKEN; //getting access token that has been saved as an environment variable
const app = express();

var smartsheet = client.createClient({ accessToken: accessToken });
var primaryColumnId = "";
var checkboxColumnId = "";

app.use(bodyParser.json()); // parse application/json

// api routes ------------------------------------------------------
// get sheet
app.get('/api/smartsheet/sheet/:sheetId', (req, res) => {
  smartsheet.sheets.getSheet({ id: req.params.sheetId }).then(function(sheet) {
    for (var i = 0; i < sheet.columns.length; i++) {
      if (sheet.columns[i].primary === true) {
        primaryColumnId = sheet.columns[i].id;
      }
      if (sheet.columns[i].type === "CHECKBOX") {
        checkboxColumnId = sheet.columns[i].id;
      }
    }
    res.send(sheet);  
  });
});

// add row
app.post('/api/smartsheet/sheet/:sheetId/row', (req, res) => {
  var row = [
    {
        "toBottom": true,
        "cells": [
            {
                "columnId": primaryColumnId,
                "value": req.body.value
            }
        ]
    }
  ];
  var options = {
    body: row,
    sheetId: req.params.sheetId
  };
  
  smartsheet.sheets.addRow(options).then(function(data) {
    res.send(data);  
  });
});

// update row
app.put('/api/smartsheet/sheet/:sheetId/row/:rowId', (req, res) => {
  var row = [
    {
        "id": req.body.value.rowId,
        "cells": [
            {
                "columnId": checkboxColumnId,
                "value": req.body.value.isDone
            },
            {
                "columnId": primaryColumnId,
                "value": req.body.value.value
            }
        ]
    }
  ];
  var options = {
    body: row,
    sheetId: req.params.sheetId
  };
  
  smartsheet.sheets.updateRow(options).then(function(data) {
    res.send(data);  
  });
});

// application routes -------------------------------------------------------------
app.get('*.js', function(req, res) {
  res.sendFile(__dirname + '/public' + req.originalUrl);
});
app.get('*.gif', function(req, res) {
  res.sendFile(__dirname + '/public' + req.originalUrl);
});
app.get('/public/js/crossoff.html', function(req, res) {
  res.sendFile(__dirname + req.originalUrl); 
});

app.get('*', function(req, res) {
    res.sendFile(__dirname + '/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.listen(3000, () => {
  console.log('Express server started on port 3000');
});