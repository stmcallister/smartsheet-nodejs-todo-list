## Smartsheet Simple To-Do List with NodeJS

As a developer, unless you've been working with blinders on for the last few years, there is a good chance that you have at least heard of the MEAN stack. This cleverly named technology stack - referring to MongoDB, Express, AngularJS, and NodeJS - has gained a lot of traction thanks to its common JavaScript thread that runs through each level of the stack. 

Taking this familiar group of technologies, we're going to tweak the name just a little. We're going to replace MongoDB with Smartsheet as the storage component. This change will give us something called a SEAN stack application.

Since we're introducing this idea here in this article, we'll give you artistic license on how it's pronounced - either "SEEN" like MEAN, or "SHAWN" like it's spelled. Your choice.

This article is going to show how you can build your first SEAN application, using the common example of a simple to-do list.

For this application, you'll want to familiarize yourself with the [Smartsheet API](https://smartsheet-platform.github.io/api-docs/) as well as the [Smartsheet Javascript SDK](https://github.com/smartsheet-platform/smartsheet-javascript-sdk). 

Also, because this is a sample application, we're going to make a few assumptions from the start. First, we'll use a generated Access Token to connect with the Smartsheet API. Smartsheet also provides Third-Party applications with the option of using [a 3-legged OAuth flow](http://smartsheet-platform.github.io/api-docs/#oauth-flow) to authorize access, but we'll tackle that in a later tutorial.

We're also going to be hardcoding the Sheet ID of our to-do list sheet. A more user friendly application would probably have something like a sheet picker, but this application is more a lesson on how the various pieces of the SEAN stack fit together.

Our file structure for this exercise will remain simple. We'll have a single `server.js` for all of our server side code. On the client, we'll create a simple Angular directive for our to-do List UI, and an Angular service for communicating with our server.

Speaking of the server, let's take a look at that code first. Like other Node applications, we're start with including the required libraries. For this application, we'll require `express`, `body-parser`, and `Smartsheet`.

    const express = require('express');
    const client = require('Smartsheet');
    const bodyParser = require('body-parser');  
    
The Smartsheet API documentation provides [instructions](http://smartsheet-platform.github.io/api-docs/#generating-access-token) on how to generate an Access Token. Once you have created your token, copy the value that is generated into your code as the value for `const accessToken`.


There are a couple of ways to get the ID of the sheet we want to use for the backend of our to-do list. One is through the API, where you could get a list of all your sheets and then find your sheet in that long list. Or, you could just grab it in the Smartsheet UI.

You can find the Sheet ID by clicking on the dropdown arrow next to the sheet title, and then selecting Properties. In the Sheet Properties window, the Sheet ID will be displayed in a handy input box. Just one click on that value and the whole string is selected for easy copy and pasting into our application code.

Using our generated Access Token, we'll create a Smartsheet client object from the included Smartsheet library. This object will be used for our three calls that will be made to the Smartsheet API.

    var smartsheet = client.createClient({ accessToken: accessToken });

Express routing is used to provide REST endpoints for the client application interact with the server. In the `server.js` file, the express routes have been grouped together by function. First, there are the api routes, which provide endpoints for making calls to the Smartsheet API. Next, are the application routes, which handle the endpoints for the client-side assets - such as html and the client-side JavaScript.

The three api endpoints needed for our application are `getSheet`,`addRow`, and `updateRow`. Let's start with `getSheet`, the most straight forward of the three endpoints.

    // get sheet
    app.get('/api/smartsheet/sheet/:sheetId', (req, res) => {
      smartsheet.sheets.getSheet({ id: req.params.sheetId }).then(function(sheet) {
        for (var i = 0; i < sheet.columns.length; i++) {
          if (sheet.columns[i].primary === true) {
            primaryColumnId = sheet.columns[i].id;
            break;
          }
        }
        res.send(sheet);  
      });
    });
    
We'll use the hardcoded sheetId value to grab our to-do list sheet with a call to `smartsheet.sheets.getSheet()`. Before sending the sheet object back to the client, we'll search the columns for the Primary Column. This column is going to hold the text of our to-dos. The `primaryColumnId` value will become especially important when we try to add or update our to-dos.

When adding a new to-do to the list, we're adding a row to our sheet on the back end. This is done by calling `smartsheet.sheets.addRow()`. This function accepts an `options` object that is made up of a combination of the `sheetId` and the `body` that will be posted. Because the `addRow` endpoint in the Smartsheet API could potentially add multiple rows, the `body` object that we'll be sending contains an array of a single row.

      var row = [
        {
            "toBottom": true,
            "cells": [
                {
                    "columnId": primaryColumnId,
                    "value": req.body.value
                }
            ]
        }
      ];
When adding a row to Smartsheet the two required values that are needed are a placement value - such as `toBottom` or `toTop` - which directs where in the sheet the row will be added, as well as an array of cells that should be included in the newly created row object.


      var options = {
        body: row,
        sheetId: req.params.sheetId
      };
  
      smartsheet.sheets.addRow(options).then(function(data) {
        res.send(data);  
      });
    });

Updating a row through the Smartsheet API is as similar to adding as a PUT is to a POST. The key difference here is in the cells array inside the row object. Now, we're updating two cells instead of one. In addition to adding a value to the cell in the Primary Column, we're also updating the cell in the checkbox column whether the checkbox has been checked on the to-do.

    var row = [
        {
            "id": req.body.value.rowId,
            "cells": [
                {
                    "columnId": req.body.value.checkboxColumnId,
                    "value": req.body.value.isDone
                },
                {
                    "columnId": primaryColumnId,
                    "value": req.body.value.value
                }
            ]
        }
      ];
      var options = {
        body: row,
        sheetId: req.params.sheetId
      };
  
      smartsheet.sheets.updateRow(options).then(function(data) {
        res.send(data);  
      });

As mentioned before, the main UI component is encapsulated in a single Angular directive called `crossoff.directive.js`. This directive references the `crossoff.html` template that makes up the UI structure, and contains the `crossoffController()` that interacts with the `smartsheet.service.js` factory to get data to and from Smartsheet. With each call to the Smartsheet service, the controller parses the response, to update the list on the browser page.

Similar to the server-side code, when we get the sheet in the client-side controller, we need to find the `primaryColumnId` as well as the `checkboxColumnId`. This is done by looping over the `columns` object returned from Smartsheet.

    vm.getTasks = function() {
	    vm.tasks = [];
        smartsheetService.getSheet(vm.sheetId).then (function (response) {
            if (response && response.data) {
                var primaryColumnId = vm.findPrimaryColumnId(response.data.columns);
                        var checkboxColumnId = vm.findCheckboxColumnId(response.data.columns);
                        var rows = response.data.rows;

For each row, we build a `task` object that will collect the values needed for updating, and keeping track of, the to-dos. Each task is initialized with the following values:
                        
                        for (var i = 0; i < rows.length; i++) {
                            var row = rows[i];
                            var task = {
                                isDone: false,
                                value: '',
                                rowId: row.id,
                                primaryColumnId: primaryColumnId,
                                checkboxColumnId: checkboxColumnId
                            };
As we loop over the cells we update the task object with the appropriate values. When we're done, we then add the object to the tasks array.

                            for (var y = 0; y < row.cells.length; y++) {
                                var cell = row.cells[y];

                                if (cell.columnId === primaryColumnId) {
                                    task.value = cell.value;
                                }
                                if (cell.columnId === checkboxColumnId && cell.value) {
                                    task.isDone = cell.value;
                                }
                            }
                            vm.tasks.push({task});
                        } 
                    }
                });

The html uses this `tasks` array to present each item in the to-do list using ng-repeat.

    <div class="checkbox" ng-repeat="task in vm.tasks">
        <label>
            <input type="checkbox" ng-click="vm.updateTask(task.task)" 
                ng-model="task.task.isDone" /> {{ task.task.value }}
        </label>
    </div>
And finally, before we send a request to the server to create a task in our to-do list, we make sure that `vm.formData.text` has a value.                    

    vm.createTask = function() {
        if (vm.formData.text != '') {
            smartsheetService.addRow(vm.sheetId,    
                vm.formData.text).then(function(response) {
                    if (response.data && response.data.message && response.data.message === "SUCCESS") {
                            initPage();
                        }
                    });
                }
            }

Now that we have an idea of how our application works, you can give it a try by running this command:
    
    node server.js
    
This will start up an Express server running on port 3000, which should be available at `http://localhost:3000/`

For more information on the Smartsheet API, sample code or SDKs visit [Smartsheet API documentation](http://smartsheet-platform.github.io/api-docs/). Development questions can also be posted to[ Stackoverflow](http://stackoverflow.com/) with the tag[ smartsheet-api](http://stackoverflow.com/questions/tagged/smartsheet-api).
